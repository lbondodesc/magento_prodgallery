<?php
/**
 * Based on /app/code/core/Mage/Adminhtml/controllers/Catalog/Product/GalleryController.php
 */
class Led_InstallationGallery_Adminhtml_Installation_Gallery_Item_GalleryController 
    extends Mage_Adminhtml_Controller_Action
{
    public function uploadAction()
    {
        try {
            $uploader = new Mage_Core_Model_File_Uploader('image');
            $uploader->setAllowedExtensions(array('jpg','jpeg','gif','png'));
            $uploader->addValidateCallback('installation_gallery_installation_gallery_item_image',
                Mage::helper('installation_gallery/image'), 'validateUploadFile');
            $uploader->setAllowRenameFiles(true);
            $uploader->setFilesDispersion(true);
            $result = $uploader->save(
                Mage::getSingleton('installation_gallery/installation_gallery_item_media_config')->getBaseTmpMediaPath()
            );
            
            /**
             * Workaround for prototype 1.7 methods "isJSON", "evalJSON" on Windows OS
             */
            $result['tmp_name'] = str_replace(DS, '/', $result['tmp_name']);
            $result['path'] = str_replace(DS, '/', $result['path']);

            $result['url'] = Mage::getSingleton('installation_gallery/installation_gallery_item_media_config')->getTmpMediaUrl($result['file']);
            $result['file'] = $result['file'] . '.tmp';
            $result['cookie'] = array(
                'name'     => session_name(),
                'value'    => $this->_getSession()->getSessionId(),
                'lifetime' => $this->_getSession()->getCookieLifetime(),
                'path'     => $this->_getSession()->getCookiePath(),
                'domain'   => $this->_getSession()->getCookieDomain()
            );

        } catch (Exception $e) {
            $result = array(
                'error' => $e->getMessage(),
                'errorcode' => $e->getCode());
        }

        $this->getResponse()->setBody(Mage::helper('core')->jsonEncode($result));
    }

}
