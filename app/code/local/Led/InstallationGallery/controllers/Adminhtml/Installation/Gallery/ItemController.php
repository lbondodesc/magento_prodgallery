<?php

class Led_InstallationGallery_Adminhtml_Installation_Gallery_ItemController 
    extends Mage_Adminhtml_Controller_Action
{

	protected function _initAction()
    {
		$this->loadLayout()
			->_setActiveMenu('installation_gallery/installation_gallery_item')
			->_addBreadcrumb(Mage::helper('adminhtml')->__('Installation Gallery Items Manager'), Mage::helper('adminhtml')->__('Installation Gallery Items Manager'));
		return $this;
	}

	public function indexAction()
    {
        $this->_initAction()
			->renderLayout();
	}

	public function editAction()
    {
    	$id     = $this->getRequest()->getParam('id');
		$model  = Mage::getModel('installation_gallery/installation_gallery_item')->load($id);
		$categoryModel = Mage::getModel('installation_gallery/installation_gallery_category')->load($model->getCategoryId());
		if ($model->getId() || $id == 0) {
			$data = Mage::getSingleton('adminhtml/session')->getFormData(true);
			if (!empty($data)) {
				$model->setData($data);
			}

            Mage::register('current_installation_gallery_item', $model);
            Mage::register('current_installation_gallery_item_category', $categoryModel);

			$this->loadLayout();
			
			$this->_setActiveMenu('installation_gallery/adminhtml_installation_gallery_item');

			$this->_addBreadcrumb(Mage::helper('installation_gallery')->__('Manage Installation Gallery Item'), Mage::helper('installation_gallery')->__('Manage Installation Gallery Items'));

			//$this->getLayout()->getBlock('head')->setCanLoadExtJs(true);

			/*
			 * $this->_addContent($this->getLayout()->createBlock('installation_gallery/adminhtml_installation_gallery_item_edit'))
				->_addLeft($this->getLayout()->createBlock('installation_gallery/adminhtml_installation_gallery_item_edit_tabs'));
            */
			$this->renderLayout();
		} else {
			Mage::getSingleton('adminhtml/session')->addError(Mage::helper('installation_gallery')->__('Item does not exist'));
			$this->_redirect('*/*/');
		}
	}

	public function newAction()
    {
		$this->_forward('edit');
	}

	public function saveAction()
    {
		if ($data = $this->getRequest()->getPost()) {
            try {
            	
                $model = Mage::getModel('installation_gallery/installation_gallery_item')
                    ->setData($data)
    				->setInstallationGalleryItemId($this->getRequest()->getParam('id'))
    				->setId($this->getRequest()->getParam('id'));
                /* Data is the stuff posted, get it with $data = $this->getRequest()->getPost() */
                if (isset($data['media_gallery'])) {
                    $media_gallery = $data['media_gallery'];
                    /**
                     * Note, the data is returned because it can contain a list of
                     * images to be deleted. Those arehandled in afterSaveMediaGallery.
                     */
                    $media_gallery = $this->beforeSaveMediaGallery($model, $media_gallery);
                }
                $model->save();
                if (isset($data['media_gallery'])) {
                    $this->afterSaveMediaGallery($model, $media_gallery);
                }
				Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('installation_gallery')->__('Item was successfully saved'));
				Mage::getSingleton('adminhtml/session')->setFormData(false);
				if ($this->getRequest()->getParam('back')) {
					$this->_redirect('*/*/edit', array('id' => $model->getId()));
					return;
				}
				$this->_redirect('*/*/');
				return;
            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
                Mage::getSingleton('adminhtml/session')->setFormData($data);
                $this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id')));
                return;
            }
        } else {
            Mage::getSingleton('adminhtml/session')->addError(Mage::helper('installation_gallery')->__('Unable to find item to save'));
            $this->_redirect('*/*/');
        }
	}

	public function deleteAction()
    {
		if ( $this->getRequest()->getParam('id') > 0 ) {
			try {
				Mage::getModel('installation_gallery/installation_gallery_item')
                    ->setId($this->getRequest()->getParam('id'))
					->delete();
				Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('adminhtml')->__('Item was successfully deleted'));
				$this->_redirect('*/*/');
			} catch (Exception $e) {
				Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
				$this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id')));
			}
		}
		$this->_redirect('*/*/');
	}

    public function massDeleteAction()
    {
        $locationIds = $this->getRequest()->getParam('installation_gallery_item');
        if (!is_array($locationIds)) {
			Mage::getSingleton('adminhtml/session')->addError(Mage::helper('adminhtml')->__('Please select item(s)'));
        } else {
            try {
                foreach ($locationIds as $locationId) {
                    $location = Mage::getModel('installation_gallery/installation_gallery_item')->load($locationId);
                    $location->delete();
                }
                Mage::getSingleton('adminhtml/session')->addSuccess(
                    Mage::helper('adminhtml')->__(
                        'Total of %d record(s) were successfully deleted', count($locationIds)
                    )
                );
            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
            }
        }
        $this->_redirect('*/*/index');
    }


    /**
     * Start based on the methods in /app/code/core/Mage/Catalog/Model/Product/Attribute/Backend/Media.php
     */
    protected $_renamedImages = array();

    protected function beforeSaveMediaGallery(Led_InstallationGallery_Model_Installation_Gallery_Item $model, $value)
    {
        if (!is_array($value) || !isset($value['images'])) {
            return;
        }

        if(!is_array($value['images']) && strlen($value['images']) > 0) {
           $value['images'] = Mage::helper('core')->jsonDecode($value['images']);
        }

        if (!is_array($value['images'])) {
           $value['images'] = array();
        }

        $clearImages = array();
        $newImages   = array();
        $existImages = array();

        /**
         * The given values are checked for files to be removed or added.
         */
        foreach ($value['images'] as &$image) {
            if(!empty($image['removed'])) {
                $clearImages[] = $image['file'];
            } else if (!isset($image['value_id'])) {
                $newFile = $this->_moveImageFromTmp($image['file']);
                $image['new_file'] = $newFile;
                $newImages[$image['file']] = $image;
                $this->_renamedImages[$image['file']] = $newFile;
                $image['file'] = $newFile;
            } else {
                $existImages[$image['file']] = $image;
            }
        }

        foreach ($model->getMediaAttributes() as $mediaAttrCode => $label) {
            $attrData = $model->getData($mediaAttrCode);

            if (in_array($attrData, $clearImages)) {
                $model->setData($mediaAttrCode, Led_InstallationGallery_Model_Installation_Gallery_Item::NO_SELECTION);
                $model->setData($mediaAttrCode.'_label', null);
            }

            if (in_array($attrData, array_keys($newImages))) {
                $model->setData($mediaAttrCode, $newImages[$attrData]['new_file']);
                $model->setData($mediaAttrCode.'_label', $newImages[$attrData]['label']);
            }

            if (in_array($attrData, array_keys($existImages))) {
                $model->setData($mediaAttrCode.'_label', $existImages[$attrData]['label']);
            }
        }

        return $value;

    }

    protected function afterSaveMediaGallery(Led_InstallationGallery_Model_Installation_Gallery_Item $model, $value)
    {

        if (!is_array($value) || !isset($value['images'])) {
            return;
        }

        $toDelete = array();
        foreach ($value['images'] as &$image) {

            if(!empty($image['removed'])) {
                if(isset($image['value_id'])) {
                    $toDelete[] = $image['value_id'];
                }
                continue;
            }
            $data = array();

            if(!isset($image['value_id'])) {
                //$data['reference_id']   = $model->getId();
                $data['installation_gallery_item_id']    = $model->getId();
                $data['path']           = $image['file'];

                $locationMedia = Mage::getModel('installation_gallery/installation_gallery_item_media');
            } else {
                $locationMedia = Mage::getModel('installation_gallery/installation_gallery_item_media')
                    ->load($image['value_id']);

                if (!is_object($locationMedia)) {
                    continue; /* Image should exist but doesn't */
                }
            }

            // Add label, position, disabled
            $data['label']    = $image['label'];
            $data['position'] = (int) $image['position'];
            $data['disabled'] = (int) $image['disabled'];

            $locationMedia
                ->addData($data)
                ->save();

        }

        foreach ($toDelete as $media_id)
        {
            $locationMedia = Mage::getModel('installation_gallery/installation_gallery_item_media')
                ->load($media_id);

            if (is_object($locationMedia))
            {
                $locationMedia->delete();
            }

        }

    }

    /**
     * Retrieve resource model
     *

     */
    protected function _getResource()
    {
        return Mage::getResourceSingleton('installation_gallery/installation_gallery_item_media');
    }

    /**
     * Retrive media config
     *
     * @return Mage_Catalog_Model_Product_Media_Config
     */
    protected function _getConfig()
    {
        return Mage::getSingleton('installation_gallery/installation_gallery_item_media_config');
    }

    /**
     * Move image from temporary directory to normal
     *
     * @param string $file
     * @return string
     */
    protected function _moveImageFromTmp($file)
    {
        $ioObject = new Varien_Io_File();
        $destDirectory = dirname($this->_getConfig()->getMediaPath($file));
        try {
            $ioObject->open(array('path'=>$destDirectory));
        } catch (Exception $e) {
            $ioObject->mkdir($destDirectory, 0777, true);
            $ioObject->open(array('path'=>$destDirectory));
        }

        if (strrpos($file, '.tmp') == strlen($file)-4) {
            $file = substr($file, 0, strlen($file)-4);
        }
        $destFile = $this->_getUniqueFileName($file, $ioObject->dirsep());

        /** @var $storageHelper Mage_Core_Helper_File_Storage_Database */
        $storageHelper = Mage::helper('core/file_storage_database');

        if ($storageHelper->checkDbUsage()) {
            $storageHelper->renameFile(
                $this->_getConfig()->getTmpMediaShortUrl($file),
                $this->_getConfig()->getMediaShortUrl($destFile));

            $ioObject->rm($this->_getConfig()->getTmpMediaPath($file));
            $ioObject->rm($this->_getConfig()->getMediaPath($destFile));
        } else {
            $ioObject->mv(
                $this->_getConfig()->getTmpMediaPath($file),
                $this->_getConfig()->getMediaPath($destFile)
            );
        }

        return str_replace($ioObject->dirsep(), '/', $destFile);
    }

    /**
     * Check whether file to move exists. Getting unique name
     *
     * @param <type> $file
     * @param <type> $dirsep
     * @return string
     */
    protected function _getUniqueFileName($file, $dirsep)
    {
        if (Mage::helper('core/file_storage_database')->checkDbUsage()) {
            $destFile = Mage::helper('core/file_storage_database')
                ->getUniqueFilename(
                    $this->_getConfig()->getBaseMediaUrlAddition(),
                    $file
                );
        } else {
            $destFile = dirname($file) . $dirsep
                . Mage_Core_Model_File_Uploader::getNewFileName($this->_getConfig()->getMediaPath($file));
        }

        return $destFile;
    }


}
