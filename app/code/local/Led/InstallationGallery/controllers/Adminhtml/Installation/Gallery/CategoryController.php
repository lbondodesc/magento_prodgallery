<?php
class Led_InstallationGallery_Adminhtml_Installation_Gallery_CategoryController 
    extends Mage_Adminhtml_Controller_Action
{

	protected function _initAction()
    {
		$this->loadLayout()
			->_setActiveMenu('installation_gallery/installation_gallery_item')
			->_addBreadcrumb(Mage::helper('adminhtml')->__('Installation Gallery Items Manager'), Mage::helper('adminhtml')->__('Installation Gallery Items Manager'));
		return $this;
	}
	
	public function indexAction()
    {
        $this->_initAction()
			->renderLayout();
	}
	
	public function editAction()
    {
    	$id     = $this->getRequest()->getParam('id');
		$model  = Mage::getModel('installation_gallery/installation_gallery_category')->load($id);
		if ($model->getId() || $id == 0) {
			$data = Mage::getSingleton('adminhtml/session')->getFormData(true);
			if (!empty($data)) {
				$model->setData($data);
			}

            Mage::register('current_installation_gallery_category', $model);

			$this->loadLayout();
			
			$this->_setActiveMenu('installation_gallery/adminhtml_installation_gallery_item');

			$this->_addBreadcrumb(Mage::helper('installation_gallery')->__('Manage Category'), Mage::helper('installation_gallery')->__('Manage Category'));

			//$this->getLayout()->getBlock('head')->setCanLoadExtJs(true);

			/*
			 * $this->_addContent($this->getLayout()->createBlock('installation_gallery/adminhtml_installation_gallery_item_edit'))
				->_addLeft($this->getLayout()->createBlock('installation_gallery/adminhtml_installation_gallery_item_edit_tabs'));
            */
			$this->renderLayout();
		} else {
			Mage::getSingleton('adminhtml/session')->addError(Mage::helper('installation_gallery')->__('Item does not exist'));
			$this->_redirect('*/*/');
		}
	}
	
	public function newAction()
    {
		$this->_forward('edit');
	}
	
	public function saveAction()
    {
		if ($data = $this->getRequest()->getPost()) {
            try {
                $model = Mage::getModel('installation_gallery/installation_gallery_category')
                    ->setData($data)
    				->setCategoryId($this->getRequest()->getParam('category_id'))
    				->setId($this->getRequest()->getParam('category_id'));
                $model->save();          
				Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('installation_gallery')->__('Item was successfully saved'));
				Mage::getSingleton('adminhtml/session')->setFormData(false);
				if ($this->getRequest()->getParam('back')) {
					$this->_redirect('*/*/edit', array('id' => $model->getId()));
					return;
				}
				$this->_redirect('*/*/');
				return;
            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
                Mage::getSingleton('adminhtml/session')->setFormData($data);
                $this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id')));
                return;
            }
        } else {
            Mage::getSingleton('adminhtml/session')->addError(Mage::helper('installation_gallery')->__('Unable to find item to save'));
            $this->_redirect('*/*/');
        }
	}
	
	public function deleteAction()
    {
		if ( $this->getRequest()->getParam('id') > 0 ) {
			try {
				Mage::getModel('installation_gallery/installation_gallery_category')
                    ->setId($this->getRequest()->getParam('id'))
					->delete();
				Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('adminhtml')->__('Item was successfully deleted'));
				$this->_redirect('*/*/');
			} catch (Exception $e) {
				Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
				$this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id')));
			}
		}
		$this->_redirect('*/*/');
	}
	
	public function massDeleteAction()
    {
        $categoryIds = $this->getRequest()->getParam('installation_gallery_category');
        dump($categoryIds);
        if (!is_array($categoryIds)) {
			Mage::getSingleton('adminhtml/session')->addError(Mage::helper('adminhtml')->__('Please select item(s)'));
        } else {
            try {
                foreach ($categoryIds as $categoryId) {
                    $category = Mage::getModel('installation_gallery/installation_gallery_category')->load($categoryId);
                    $category->delete();
                }
                Mage::getSingleton('adminhtml/session')->addSuccess(
                    Mage::helper('adminhtml')->__(
                        'Total of %d record(s) were successfully deleted', count($categoryIds)
                    )
                );
            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
            }
        }
        $this->_redirect('*/*/index');
    }
	
}