<?php

$installer = $this;
/** @var $installer Mage_Catalog_Model_Resource_Setup */

$installer->startSetup();

/**
 * Create table 'installation_gallery_item_category'
 */
$table = $installer->getConnection()
    ->newTable($installer->getTable('installation_gallery/installation_gallery_category'))
    ->addColumn('category_id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
        'identity' => true,
        'unsigned' => true,
        'nullable' => false,
        'primary' => true,
        ), 'Category ID')
    ->addColumn('category_name', Varien_Db_Ddl_Table::TYPE_TEXT, 255, array(
      	), 'Category Name')
    ->setComment('Installation Gallery Item Category');

$installer->getConnection()->createTable($table);

/**
 * Create table 'installation_gallery'
 */
$table = $installer->getConnection()
    ->newTable($installer->getTable('installation_gallery/installation_gallery_item'))
    ->addColumn('installation_gallery_item_id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
        'identity'  => true,
        'unsigned'  => true,
        'nullable'  => false,
        'primary'   => true,
        ), 'Installation Gallery Item Id')
    ->addColumn('installation_gallery_item_type', Varien_Db_Ddl_Table::TYPE_VARCHAR, 50, array(
        'nullable' => false,
   		'default'   => '',
        ), 'Installation Gallery Item Type')
    ->addColumn('category_id', Varien_Db_Ddl_Table::TYPE_SMALLINT, null, array(
        'unsigned' => true,
        'nullable' => true,
        ), 'Installation Gallery Category ID')
    ->addColumn('installation_gallery_item_name', Varien_Db_Ddl_Table::TYPE_VARCHAR, 50, array(
        'nullable'  => false,
        'default'   => '',
        ), 'Installation Gallery Name')
    ->addColumn('products_installed_before', Varien_Db_Ddl_Table::TYPE_VARCHAR, 250, array(
        'nullable'  => true,
        'default'   => '',
        ), 'Products Installed Before')
    ->addColumn('goodlight_replacement', Varien_Db_Ddl_Table::TYPE_VARCHAR, 250, array(
        'nullable'  => true,
        'default'   => '',
        ), 'Good light Replacement')
    ->addColumn('annual_energy_saving', Varien_Db_Ddl_Table::TYPE_FLOAT, null, array(
        'nullable'  => true,
        'default'   => '0',
        ), 'Annual Energy Saving')
    ->addColumn('annual_financial_saving', Varien_Db_Ddl_Table::TYPE_FLOAT, null, array(
        'nullable'  => true,
        'default'   => '0',
        ), 'Annual Financial Saving')
    ->addColumn('annual_carbon_saving', Varien_Db_Ddl_Table::TYPE_FLOAT, null, array(
        'nullable'  => true,
        'default'   => '0',
        ), 'Annual Carbon Saving')
    ->addColumn('installation_gallery_item_description', Varien_Db_Ddl_Table::TYPE_TEXT, null, array(
        'nullable'  => true,
        'default'   => '',
        ), 'Installation Gallery Item Description')
    ->addColumn('position', Varien_Db_Ddl_Table::TYPE_SMALLINT, null, array(
        'unsigned'  => true,
        'nullable'  => false,
        'default'   => '0',
        ), 'Position')
    ->addColumn('image', Varien_Db_Ddl_Table::TYPE_TEXT, 255, array(
        'nullable' => true,
        'default' => Led_InstallationGallery_Model_Installation_Gallery_Item::NO_SELECTION,
        'comment' => 'Installation Gallery Item base image',
        ), 'Base Image')
    ->addColumn('image_label', Varien_Db_Ddl_Table::TYPE_TEXT, 255, array(
        'nullable' => true,
        'comment' => 'Installation Gallery Item base image label',
        ), 'Base Image Label')
    ->addColumn('small_image', Varien_Db_Ddl_Table::TYPE_TEXT, 255, array(
        'nullable' => true,
        'default' => Led_InstallationGallery_Model_Installation_Gallery_Item::NO_SELECTION,
        'comment' => 'My moodel small image',
        ), 'Small Image')
    ->addColumn('small_image_label', Varien_Db_Ddl_Table::TYPE_TEXT, 255, array(
        'nullable' => true,
        'comment' => 'Installation Gallery Item small image label',
        ), 'Small Image Label')
    ->addColumn('thumbnail', Varien_Db_Ddl_Table::TYPE_TEXT, 255, array(
        'nullable' => true,
        'default' => Led_InstallationGallery_Model_Installation_Gallery_Item::NO_SELECTION,
        'comment' => 'Installation Gallery Item thumbnail',
        ), 'Thumbnail')
    ->addColumn('thumbnail_label', Varien_Db_Ddl_Table::TYPE_TEXT, 255, array(
        'nullable' => true,
        'comment' => 'Installation Gallery Item thumbnail label',
        ), 'Thumbnail Label')
    /*
    ->addForeignKey($installer->getFkName('installation_gallery/installation_gallery_item', 'category_id', 'installation_gallery/installation_gallery_category', 'category_id'), 
        'category_id', $installer->getTable('installation_gallery/installation_gallery_category'), 'category_id', 
        Varien_Db_Ddl_Table::ACTION_CASCADE, Varien_Db_Ddl_Table::ACTION_CASCADE)
    */    
    ->setComment('Led Installation Gallery Item');
$installer->getConnection()->createTable($table);

///Media table
$table = $installer->getConnection()
    ->newTable($installer->getTable('installation_gallery/installation_gallery_item_media'))
    ->addColumn('media_id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
        'identity' => true,
        'unsigned' => true,
        'nullable' => false,
        'primary' => true,
        ), 'Value ID')
    ->addColumn('installation_gallery_item_id', Varien_Db_Ddl_Table::TYPE_SMALLINT, null, array(
        'unsigned' => true,
        'nullable' => false,
        ), 'installation_gallery ID')
    ->addColumn('path', Varien_Db_Ddl_Table::TYPE_TEXT, 255, array(
        ), 'Path')
    ->addColumn('label', Varien_Db_Ddl_Table::TYPE_TEXT, 255, array(
        ), 'Label')
    ->addColumn('position', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
        'unsigned' => true,
        ), 'Position')
    ->addColumn('disabled', Varien_Db_Ddl_Table::TYPE_SMALLINT, null, array(
        'unsigned' => true,
        'nullable' => false,
        'default' => '0',
        ), 'Is Disabled')
    ->addColumn('creation_time', Varien_Db_Ddl_Table::TYPE_TIMESTAMP, null, array(
        ), 'Media Creation Time')
    ->addColumn('update_time', Varien_Db_Ddl_Table::TYPE_TIMESTAMP, null, array(
        ), 'Media Modification Time')
    ->addForeignKey($installer->getFkName('installation_gallery/installation_gallery_item_media', 'installation_gallery_item_id', 'installation_gallery/installation_gallery_item', 'installation_gallery_item_id'), 
        'installation_gallery_item_id', $installer->getTable('installation_gallery/installation_gallery_item'), 'installation_gallery_item_id', 
        Varien_Db_Ddl_Table::ACTION_CASCADE, Varien_Db_Ddl_Table::ACTION_CASCADE)
    ->setComment('installation_gallery Media');

$installer->getConnection()->createTable($table);
