<?php

class Led_InstallationGallery_Block_Adminhtml_Installation_Gallery_Item_Edit extends Mage_Adminhtml_Block_Widget_Form_Container
{

    public function __construct()
    {
        parent::__construct();
	
        $this->_objectId = 'id';
        $this->_blockGroup = 'installation_gallery';
        $this->_controller = 'adminhtml_installation_gallery_item';
	
        $this->_updateButton('save', 'label', Mage::helper('installation_gallery')->__('Save Installation Gallery Item'));
        $this->_updateButton('delete', 'label', Mage::helper('installation_gallery')->__('Delete Installation Gallery Item'));
			
        $this->_addButton('saveandcontinue', array(
            'label'     => Mage::helper('adminhtml')->__('Save And Continue Edit'),
            'onclick'   => 'saveAndContinueEdit()',
            'class'     => 'save',
        ), -100);
       
       // $id = Mage::registry('current_installation_gallery_item')->getId();
        $this->_formScripts[] = "
            function saveAndContinueEdit(){
                editForm.submit($('edit_form').action+'back/edit/');
            }
        ";
        return $this;
    }

    protected function _getInstallationGalleryItem()
    {
        return Mage::registry('current_installation_gallery_item');
    }

    public function getHeaderText()
    {
        if( $this->_getInstallationGalleryItem() && $this->_getInstallationGalleryItem()->getId() ) {
            return Mage::helper('installation_gallery')->__("Edit Installation Gallery Item '%s'", $this->htmlEscape($this->_getInstallationGalleryItem()->getInstallationGalleryItemName()));
        } else {
            return Mage::helper('installation_gallery')->__('Add Installation Gallery Item');
        }
    }


}