<?php

class Led_InstallationGallery_Block_Adminhtml_Installation_Gallery_Item_Edit_Tab_Form 
    extends Mage_Adminhtml_Block_Widget_Form
    implements Mage_Adminhtml_Block_Widget_Tab_Interface
{
    
    public function __construct()
    {
        parent::__construct();
    }
    
	protected function _prepareLayout()
    {
        parent::_prepareLayout();
        
        if (Mage::getSingleton('cms/wysiwyg_config')->isEnabled()) {
            $this->getLayout()->getBlock('head')->setCanLoadTinyMce(true);
        }
    }

    public function _prepareForm()
    {
    	dump(Mage::getSingleton('installation_gallery/installation_gallery_category')->toOptionArray(true));
    	dump($this->_getInstallationGalleryItemCategory()->getCategoryId());
        $form = new Varien_Data_Form();
        $form->setHtmlIdPrefix('installation_gallery_item_');

        $wysiwygConfig = Mage::getSingleton('cms/wysiwyg_config')->getConfig(
            array('tab_id' => $this->getTabId())
        );

        $fieldset = $form->addFieldset('installation_gallery_item_form', array(
            'legend' => Mage::helper('installation_gallery')->__('Installation information'),'class'=>'fieldset-wide'
        ));
       
      
        $fieldset->addField('installation_gallery_item_name', 'text', array(
            'label'     => Mage::helper('installation_gallery')->__('Name'),
            'class'     => 'required-entry',
            'required'  => true,
            'name'      => 'installation_gallery_item_name',
        ));
        /*
        $fieldset->addField('is_case_study', 'checkbox', array(
            'label'     => Mage::helper('installation_gallery')->__('Is Case Study'),
            //'class'     => 'validate-select', //required-entry
          //  'value'    => '1',
        	'checked'	=> intval($this->_getInstallationGalleryItem()->getIsCaseStudy()) ? true : false,
            'required'  => false,
            'name'      => 'is_case_study',
        ));
        */
        $fieldset->addField('installation_gallery_item_type', 'select', array(
            'label'     => Mage::helper('installation_gallery')->__('Type'),
            'value'  => '1',
            'values' => array('installation' => 'Installation','case_study' => 'Case Study'),
            'value'     => $this->_getInstallationGalleryItem()->getInstallationGalleryItemType(),
        	'name'      => 'installation_gallery_item_type'
        ));
        
        $fieldset->addField('installation_gallery_item_category', 'select', array(
            'label'    => Mage::helper('igallery')->__('Category'),
            'values'   => Mage::getSingleton('installation_gallery/installation_gallery_category')->toOptionArray(true),
            'value'    =>  '1',//$this->_getInstallationGalleryItemCategory()->getCategoryId(),
            'name'     => 'category_id'
        ));
        
         $fieldset->addField('position', 'text', array(
            'label'     => Mage::helper('installation_gallery')->__('Position'),
            'class'     => 'validate-digits', // validate-zero-or-greater
            'required'  => false,
            'name'      => 'position',
        ));

         $fieldset->addField('installation_gallery_item_description', 'editor', array(
            'label'     => Mage::helper('installation_gallery')->__('Installation Description'),
            'title'     => Mage::helper('installation_gallery')->__('Installation Description'),
            'style'     => 'height:10em',
            'required'  => false,
         	'wysiwyg' => true, 
            'config'    => $wysiwygConfig,
            'name'      => 'installation_gallery_item_description'
        ));
        
        $fieldset->addField('products_installed_before', 'text', array(
            'label'     => Mage::helper('installation_gallery')->__('Products Installed Before'),
            'required'  => false,
            'name'      => 'products_installed_before',
        ));
        
        $fieldset->addField('goodlight_replacement', 'text', array(
            'label'     => Mage::helper('installation_gallery')->__('Good light Replacement'),
            'required'  => false,
            'name'      => 'goodlight_replacement',
        ));
        
        $fieldset->addField('annual_energy_saving', 'text', array(
            'label'     => Mage::helper('installation_gallery')->__('Annual Energy Saving'),
			'class'     => 'validate-number', // validate-zero-or-greater
        	'required'  => false,
            'name'      => 'annual_energy_saving',
        ));
        
        $fieldset->addField('annual_financial_saving', 'text', array(
            'label'     => Mage::helper('installation_gallery')->__('Annual Financial Saving'),
			'class'     => 'validate-number', // validate-zero-or-greater
        	'required'  => false,
            'name'      => 'annual_financial_saving',
        ));
        
        $fieldset->addField('annual_carbon_saving', 'text', array(
            'label'     => Mage::helper('installation_gallery')->__('Annual Carbon Saving'),
			'class'     => 'validate-number', // validate-zero-or-greater
        	'required'  => false,
            'name'      => 'annual_carbon_saving',
        ));
        

        if ( Mage::getSingleton('adminhtml/session')->getInstallationGalleryItemData() ) {
            $form->setValues(Mage::getSingleton('adminhtml/session')->getInstallationGalleryItemData());
            Mage::getSingleton('adminhtml/session')->setInstallationGalleryItemData(null);
        } elseif ( $this-> _getInstallationGalleryItem() ) {
            $data = $this-> _getInstallationGalleryItem()->getData();
            $form->setValues($data);
        }
        
        $this->setForm($form);
        return $this;
    }

    /**
     * Prepare label for tab
     *
     * @return string
     */
    public function getTabLabel()
    {
        return Mage::helper('cms')->__('Installation Item Information');
    }

    /**
     * Prepare title for tab
     *
     * @return string
     */
    public function getTabTitle()
    {
        return Mage::helper('cms')->__('Installation Item Information');
    }

    /**
     * Returns status flag about this tab can be shown or not
     *
     * @return true
     */
    public function canShowTab()
    {
        return true;
    }

    /**
     * Returns status flag about this tab hidden or not
     *
     * @return true
     */
    public function isHidden()
    {
        return false;
    }

    protected function _getInstallationGalleryItem()
    {
        return Mage::registry('current_installation_gallery_item');
    }
    
	protected function _getInstallationGalleryItemCategory()
    {
        return Mage::registry('current_installation_gallery_item_category');
    }


}