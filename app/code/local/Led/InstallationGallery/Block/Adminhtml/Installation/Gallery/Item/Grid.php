<?php

class Led_InstallationGallery_Block_Adminhtml_Installation_Gallery_Item_Grid extends Mage_Adminhtml_Block_Widget_Grid
{

    public function __construct()
    {
        parent::__construct();
        $this->setId('installationGalleryItemGrid');
        $this->setDefaultSort('position');
        $this->setDefaultDir('ASC');
        $this->setSaveParametersInSession(true);   
    }
    
    protected function _prepareCollection()
    {
        $collection = Mage::getModel('installation_gallery/installation_gallery_item')->getCollection();
        
        $this->setCollection($collection);
        return parent::_prepareCollection();
    }
    
    protected function _prepareColumns()
    {
    	/*
        $this->addColumn('position', array(
            'header'    => Mage::helper('installation_gallery')->__('Position'),
            'align'     => 'left',
            'width'     => '100px',
            'index'     => 'position',
            'filter'    => false,
        ));
        */
        $this->addColumn('installation_gallery_item_name', array(
            'header'    => Mage::helper('installation_gallery')->__('Installation Gallery Item Name'),
            'align'     => 'left',
            'index'     => 'installation_gallery_item_name',
        ));
        
        /*
         $this->addColumn('category_id', array(
            'header'    => Mage::helper('installation_gallery')->__('Category'),
            'type'      => 'text',
            'align'     => 'left',
            'index'     => 'category_id',
        ));
        */
        
        $this->addColumn('installation_gallery_item_type',
            array(
                'header'=>Mage::helper('installation_gallery')->__('Type'),
                'index'=>'installation_gallery_item_type',
                'type' => 'options',
                'options' => array('installation' => 'Installation', 'case_study' => 'Case Study')
        ));
        
   		$this->addColumn('category_name',
            array(
                'header'=>Mage::helper('installation_gallery')->__('Category Name'),
                'index'=>'category_id',
                'type' => 'options',
                'options' => $this->_getInstallationGalleryItemCategory()->getCategoryOptions()
        ));
        
        $this->addColumn('action', array(
            'header'    =>  Mage::helper('installation_gallery')->__('Action'),
            'width'     => '100',
            'type'      => 'action',
            'getter'    => 'getInstallationGalleryItemId',
            'actions'   => array(
                array(
                    'caption'   => Mage::helper('installation_gallery')->__('Edit'),
                    'url'       => array('base'=> '*/*/edit'),
                    'field'     => 'id'
                ),
                array(
                    'caption'   => Mage::helper('installation_gallery')->__('Delete'),
                    'url'       => array('base'=> '*/*/delete'),
                    'field'     => 'id',
                    'confirm'   => Mage::helper('installation_gallery')->__('Are you sure?'),
                )
            ),
            'filter'    => false,
            'sortable'  => false,
            'index'     => 'stores',
            'is_system' => true,
        ));

        return $this;
    }
    
    protected function _prepareMassaction()
    {
        $this->setMassactionIdField('installation_gallery_item_id');
        $this->getMassactionBlock()->setFormFieldName('installation_gallery_item');
        
        $this->getMassactionBlock()->addItem('delete', array(
            'label'    => Mage::helper('installation_gallery')->__('Delete'),
            'url'      => $this->getUrl('*/*/massDelete'),
            'confirm'  => Mage::helper('installation_gallery')->__('Are you sure?')
        ));
        
        return $this;
    }
    
    public function getRowUrl($row)
    {
        return $this->getUrl('*/*/edit', array('id' => $row->getId()));
    }
    
	protected function _getInstallationGalleryItemCategory()
    {
        return Mage::getModel('installation_gallery/installation_gallery_category');
    }
	
    protected function _getInstallationGalleryItem()
    {
        return Mage::getModel('installation_gallery/installation_gallery_item');
    }


}