<?php

class Led_InstallationGallery_Block_Adminhtml_Installation_Gallery_Item_Edit_Tabs 
    extends Mage_Adminhtml_Block_Widget_Tabs
{

    public function __construct()
    {
        parent::__construct();
        $this->setId('installation_gallery_item_tabs');
        $this->setDestElementId('edit_form');
        $this->setTitle(Mage::helper('installation_gallery')->__('Installation Gallery'));
    }
    
    protected function _beforeToHtml()
    {

        if ($tab = $this->getRequest()->getParam('tab', '')) {
            $this->setActiveTab($tab);
        }

        return parent::_beforeToHtml();
    }

    protected function _getInstallationGalleryItem()
    {
        return Mage::registry('current_installation_gallery_item');
    }


}