<?php

class Led_InstallationGallery_Block_Adminhtml_Installation_Gallery_Item_Edit_Tab_Media
    extends Mage_Adminhtml_Block_Widget_Form
    implements Mage_Adminhtml_Block_Widget_Tab_Interface
{
    
    public function __construct()
    {
        parent::__construct();
    }

    protected function _prepareForm()
    {
        $model = $this->_getInstallationGalleryItem(); /* This value is registered in the mycontroller/myaction action and is basically the model for the posted model_Id.*/

        /*
         * Checking if user have permissions to save information
         */
        if ($this->_isAllowedAction('save')) {
            $isElementDisabled = false;
        } else {
            $isElementDisabled = true;
        }

        $form = new Varien_Data_Form();

        /**
         * Initialize reference object as form property
         * for using it in elements generation
         */
        $form->setDataObject($model);

        $form->setHtmlIdPrefix('installation_gallery_item_');

        $fieldset = $form->addFieldset('media_fieldset', array('legend'=>Mage::helper('installation_gallery')->__('Installation Gallery Item Gallery'), 'class' => 'fieldset-wide'));

        $fieldset->addType('media_gallery', Mage::getConfig()->getBlockClassName('installation_gallery/adminhtml_installation_gallery_item_helper_form_gallery'));
		
        
        ///Error -- @text@ to @media_gallery@
        $fieldset->addField('media_gallery', 'media_gallery', array(
            'name' => 'media_gallery',
            'label' => Mage::helper('installation_gallery')->__('Installation Gallery Item Gallery'),
            'title' => Mage::helper('installation_gallery')->__('Installation Gallery Item Gallery'),
            'disabled'  => $isElementDisabled,
        ));

        $form->setValues($model->getData());

        $this->setForm($form);

        return parent::_prepareForm();
    }

    /**
     * Prepare label for tab
     *
     * @return string
     */
    public function getTabLabel()
    {
        return Mage::helper('installation_gallery')->__('Item Gallery');
    }

    /**
     * Prepare title for tab
     *
     * @return string
     */
    public function getTabTitle()
    {
        return Mage::helper('installation_gallery')->__('Item Gallery');
    }

    /**
     * Returns status flag about this tab can be showen or not
     *
     * @return true
     */
    public function canShowTab()
    {
        return true;
    }

    /**
     * Returns status flag about this tab hidden or not
     *
     * @return true
     */
    public function isHidden()
    {
        return false;
    }

    /**
     * Check permission for passed action
     *
     * @param string $action
     * @return bool
     */
    protected function _isAllowedAction($action)
    {
        return Mage::getSingleton('admin/session')->isAllowed('installation_gallery/installation_gallery_item/' . $action);
    }

    protected function _getInstallationGalleryItem()
    {
        return Mage::registry('current_installation_gallery_item');
    }

}
