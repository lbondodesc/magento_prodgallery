<?php
class Led_InstallationGallery_Block_Adminhtml_Installation_Gallery_Category 
    extends Mage_Adminhtml_Block_Widget_Grid_Container
{
    
    public function __construct()
    {
        $this->_controller = 'adminhtml_installation_gallery_category';
        $this->_blockGroup = 'installation_gallery';
        $this->_headerText = Mage::helper('installation_gallery')->__('Installation Category');
        $this->_addButtonLabel = Mage::helper('installation_gallery')->__('Add Installation Category');
        parent::__construct();
    }
    
}