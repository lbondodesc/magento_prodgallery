<?php

class Led_InstallationGallery_Block_Adminhtml_Installation_Gallery_Category_Grid extends Mage_Adminhtml_Block_Widget_Grid
{

    public function __construct()
    {
        parent::__construct();
        $this->setId('installationGalleryCategoryGrid');
        $this->setDefaultSort('position');
        $this->setDefaultDir('ASC');
        $this->setSaveParametersInSession(true);   
    }
    
    protected function _prepareCollection()
    {
        $collection = Mage::getModel('installation_gallery/installation_gallery_category')->getCollection();
        
        $this->setCollection($collection);
        return parent::_prepareCollection();
    }
    
    protected function _prepareColumns()
    {
       
        $this->addColumn('category_name', array(
            'header'    => Mage::helper('installation_gallery')->__('Category Name'),
            'align'     => 'left',
            'index'     => 'category_name',
        ));
        
        $this->addColumn('action', array(
            'header'    =>  Mage::helper('installation_gallery')->__('Action'),
            'width'     => '100',
            'type'      => 'action',
            'getter'    => 'getCategoryId',
            'actions'   => array(
                array(
                    'caption'   => Mage::helper('installation_gallery')->__('Edit'),
                    'url'       => array('base'=> '*/*/edit'),
                    'field'     => 'id'
                ),
                array(
                    'caption'   => Mage::helper('installation_gallery')->__('Delete'),
                    'url'       => array('base'=> '*/*/delete'),
                    'field'     => 'id',
                    'confirm'   => Mage::helper('installation_gallery')->__('Are you sure?'),
                )
            ),
            'filter'    => false,
            'sortable'  => false,
            'index'     => 'stores',
            'is_system' => true,
        ));

        return $this;
    }
    
    protected function _prepareMassaction()
    {
        $this->setMassactionIdField('category_id');
        $this->getMassactionBlock()->setFormFieldName('installation_gallery_category');
        
        $this->getMassactionBlock()->addItem('delete', array(
            'label'    => Mage::helper('installation_gallery')->__('Delete'),
            'url'      => $this->getUrl('*/*/massDelete'),
            'confirm'  => Mage::helper('installation_gallery')->__('Are you sure?')
        ));
        
        return $this;
    }
    
    public function getRowUrl($row)
    {
        return $this->getUrl('*/*/edit', array('id' => $row->getId()));
    }


}