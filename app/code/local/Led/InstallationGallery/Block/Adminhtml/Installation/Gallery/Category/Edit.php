<?php
class Led_InstallationGallery_Block_Adminhtml_Installation_Gallery_Category_Edit extends Mage_Adminhtml_Block_Widget_Form_Container
{

    public function __construct()
    {
        parent::__construct();
	
        $this->_objectId = 'id';
        $this->_blockGroup = 'installation_gallery';
        $this->_controller = 'adminhtml_installation_gallery_category';
	
        $this->_updateButton('save', 'label', Mage::helper('installation_gallery')->__('Save Category'));
        $this->_updateButton('delete', 'label', Mage::helper('installation_gallery')->__('Delete Category'));
			
        $this->_addButton('saveandcontinue', array(
            'label'     => Mage::helper('adminhtml')->__('Save And Continue Edit'),
            'onclick'   => 'saveAndContinueEdit()',
            'class'     => 'save',
        ), -100);
       
       // $id = Mage::registry('current_installation_gallery_item')->getId();
        $this->_formScripts[] = "
            function saveAndContinueEdit(){
                editForm.submit($('edit_form').action+'back/edit/');
            }
        ";
        return $this;
    }

    protected function _getInstallationGalleryCategory()
    {
        return Mage::registry('current_installation_gallery_category');
    }

    public function getHeaderText()
    {
    	
        if( $this->_getInstallationGalleryCategory() && $this->_getInstallationGalleryCategory()->getId() ) {
            return Mage::helper('installation_gallery')->__("Edit Category '%s'", $this->htmlEscape($this->_getInstallationGalleryCategory()->getCategoryName()));
        } else {
            return Mage::helper('installation_gallery')->__('Add Category');
        }
    }


}