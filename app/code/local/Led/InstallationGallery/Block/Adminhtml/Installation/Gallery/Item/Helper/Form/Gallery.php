<?php

/**
 * Based on /app/code/core/Mage/Adminhtml/Block/Catalog/Product/Helper/Form/Gallery.php
 *
 * Note: for translations, the catalog helper is used.
 */
class Led_InstallationGallery_Block_Adminhtml_Installation_Gallery_Item_Helper_Form_Gallery 
    extends Varien_Data_Form_Element_Abstract
{

    public function getElementHtml()
    {
        $html = $this->getContentHtml();
        return $html;
    }

    /**
     * Prepares content block
     *
     * @return string
     */
    public function getContentHtml()
    {

        $content = Mage::getSingleton('core/layout')
            ->createBlock('Led_InstallationGallery_Block_Adminhtml_Installation_Gallery_Item_Helper_Form_Gallery_Content');
        $content->setId($this->getHtmlId() . '_content')
            ->setElement($this);
		
        return $content->toHtml();
    }

    public function getLabel()
    {
        return '';
    }

    /**
     * Retrieve data object related with form
     *
     * @return Mage_Catalog_Model_Product || Mage_Catalog_Model_Category
     */
    public function getDataObject()
    {
        return $this->getForm()->getDataObject();
    }

    /**
     * Retrieve attribute field name
     *
     *
     * @param array $attribute ( code => label )
     * @return string
     */
    public function getAttributeFieldName($attribute)
    {
        $name = key($attribute);
        if ($suffix = $this->getForm()->getFieldNameSuffix()) {
            $name = $this->getForm()->addSuffixToName($name, $suffix);
        }
        
        return $name;
    }

    public function toHtml()
    {
        return '<tr><td class="value" colspan="3">' . $this->getElementHtml() . '</td></tr>';
    }

}
