<?php

class Led_InstallationGallery_Block_Adminhtml_Installation_Gallery_Item 
    extends Mage_Adminhtml_Block_Widget_Grid_Container
{
    
    public function __construct()
    {
        $this->_controller = 'adminhtml_installation_gallery_item';
        $this->_blockGroup = 'installation_gallery';
        $this->_headerText = Mage::helper('installation_gallery')->__('Installation Gallery Items Manager');
        $this->_addButtonLabel = Mage::helper('installation_gallery')->__('Add Installation Gallery Item');
        parent::__construct();
    }
    
}