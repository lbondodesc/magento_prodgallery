<?php
class Led_InstallationGallery_Block_Adminhtml_Installation_Gallery_Category_Edit_Form
    extends Mage_Adminhtml_Block_Widget_Form
{

	protected function _prepareForm()
    {
        $category = Mage::registry('current_installation_gallery_category');
        $form = new Varien_Data_Form();
        $fieldset = $form->addFieldset('edit_category', array(
            'legend' => Mage::helper('installation_gallery')->__('Category Details')
        ));

        if ($category->getId()) {
            $fieldset->addField('category_id', 'hidden', array(
                'name'      => 'category_id',
                'required'  => true
            ));
        }
 
        $fieldset->addField('category_name', 'text', array(
            'name'      => 'category_name',
            'label'     => Mage::helper('installation_gallery')->__('Category Name'),
            'maxlength' => '250',
            'required'  => true,
        ));
        
        $form->setMethod('post');
        $form->setUseContainer(true);
        $form->setId('edit_form');
        $form->setAction($this->getUrl('*/*/save'));
        $form->setValues($category->getData());
 
        $this->setForm($form);
        
        return parent::_prepareForm();
    }
    
}