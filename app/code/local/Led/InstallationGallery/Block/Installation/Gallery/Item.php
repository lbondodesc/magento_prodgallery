<?php

class Led_InstallationGallery_Block_Installation_Gallery_Item extends Mage_Core_Block_Template
{

	public function __construct()
    {
		parent::__construct();
        $itemId = Mage::app()->getRequest()->getParam('item_id');
        if (ctype_digit($itemId)) {
            $this->setTemplate('led/installation_gallery/item.phtml');
        } else {
            $this->setTemplate('led/installation_gallery/gallery.phtml');
        }
    }

    /**
     * Preparing global layout
     *
     * @return Mage_Core_Block_Abstract
     */
    protected function _prepareLayout()
    {
        return parent::_prepareLayout();
    }

    public function getCollection()
    {
        if (!$this->hasData('collection')) {
            $collection = Mage::getModel('installation_gallery/installation_gallery_item')->getCollection()
                ->addOrder('position', Varien_Data_Collection_Db::SORT_ORDER_ASC);
            $this->setData('collection', $collection);
        }
        return $this->getData('collection');
    }

    public function getCollectionsIds()
    {
        $collection = Mage::getModel('installation_gallery/installation_gallery_item')->getCollection()
            ->removeAllFieldsFromSelect()
            ->addFieldToSelect('installation_gallery_item_id')
            ->addOrder('position', Varien_Data_Collection_Db::SORT_ORDER_ASC);
        return $collection->getData();
    }

    public function getNeighborIds($id)
    {
        $arrayId = $this->getCollectionsIds();
        $currentId = FALSE;
        foreach ($arrayId as $key => $item) {
            if ($item['installation_gallery_item_id'] == $id) {
                $currentId = $key;
                break;
            }
        }
        $neighbors = array();
        $neighbors['prev'] = ($currentId > 0) ? ((int)$currentId - 1) : false;
        $neighbors['curr'] = $currentId;
        $neighbors['next'] = (($currentId < (count($arrayId) - 1)) && ($currentId !== FALSE))
                           ? ($currentId + 1)
                           : false;
        return $neighbors;
    }

    public function getInstallationGaleryItemById($id)
    {
        $galleryItem = Mage::getModel('installation_gallery/installation_gallery_item')->load((int)$id);
        return $galleryItem;
    }

    public function getInstallationGalleryItem()
    {
        if (!$this->hasData('installation_gallery_item')) {
            $this->setData('installation_gallery_item', Mage::registry('installation_gallery_item'));
        }
        return $this->getData('installation_gallery_item');

    }

    public function getInstallationGalleryItemMediaUrl($file)
    {
        return Mage::getSingleton('installation_gallery/installation_gallery_item_media_config')->getMediaUrl($file);
    }

    public function getItemUrl($id)
    {
        $return = 'item_id=' . $id;
        if (strpos(Mage::app()->getStore()->getCurrentUrl(false),'?')) {
            $return = Mage::app()->getStore()->getCurrentUrl(false) . '&' . $return;
        } elseif (strrpos(Mage::app()->getStore()->getCurrentUrl(false),'/') === strlen(Mage::app()->getStore()->getCurrentUrl(false))) {
            $return = Mage::app()->getStore()->getCurrentUrl(false) . '?' . $return;
        } else {
            $return = Mage::app()->getStore()->getCurrentUrl(false) . '/?' . $return;
        }
        return $return;
    }
}
