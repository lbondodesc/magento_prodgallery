<?php
/**
 * Based on /app/code/core/Mage/Catalog/Helper/Image.php
 */
class Led_InstallationGallery_Helper_Image 
    extends Mage_Core_Helper_Abstract
{

    public function init(Mage_Catalog_Model_Product_Image $model, $attributeName, $imageFile=null) 
    {
        $this->_reset();
        $this->_setImageModel($model->getImageModel());
        $this->_getImageModel()->setDestinationSubdir($attributeName);
        $this->setM($model);

        $this->setWatermark(Mage::getStoreConfig("design/watermark/{$this->_getImageModel()->getDestinationSubdir()}_image"));
        $this->setWatermarkImageOpacity(Mage::getStoreConfig("design/watermark/{$this->_getImageModel()->getDestinationSubdir()}_imageOpacity"));
        $this->setWatermarkPosition(Mage::getStoreConfig("design/watermark/{$this->_getImageModel()->getDestinationSubdir()}_position"));
        $this->setWatermarkSize(Mage::getStoreConfig("design/watermark/{$this->_getImageModel()->getDestinationSubdir()}_size"));

        if ($imageFile) {
            $this->setImageFile($imageFile);
        }
        else {
            // add for work original size
            $this->_getImageModel()->setBaseFile( $this->getReference()->getData($this->_getImageModel()->getDestinationSubdir()) );
        }
        return $this;
    }

    public function __toString()
    {
        try {
            if( $this->getImageFile() ) {
                $this->_getImageModel()->setBaseFile( $this->getImageFile() );
            } else {
                $this->_getImageModel()->setBaseFile( $this->getReference()->getData($this->_getImageModel()->getDestinationSubdir()) );
            }

            if( $this->_getImageModel()->isCached() ) {
                return $this->_getImageModel()->getUrl();
            } else {
                if( $this->_scheduleRotate ) {
                    $this->_getImageModel()->rotate( $this->getAngle() );
                }

                if ($this->_scheduleResize) {
                    $this->_getImageModel()->resize();
                }

                if( $this->getWatermark() ) {
                    $this->_getImageModel()->setWatermark($this->getWatermark());
                }

                $url = $this->_getImageModel()->saveFile()->getUrl();
            }
        } catch( Exception $e ) {
            $url = Mage::getDesign()->getSkinUrl($this->getPlaceholder());
        }
        return $url;
    }


}
