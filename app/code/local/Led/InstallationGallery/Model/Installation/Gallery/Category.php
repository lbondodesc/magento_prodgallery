<?php
class Led_InstallationGallery_Model_Installation_Gallery_Category 
    extends Mage_Core_Model_Abstract
{

    public function _construct()
    {
        parent::_construct();
        $this->_init('installation_gallery/category');
    }
    
    public function toOptionArray($withEmpty = false)
    {
        $options = array();

        foreach ($this->getCollection() as $value => $label) {
            $options[] = array(
                'label' => $label->getCategoryName(),
                'value' => $value
            );
        }
        
        if ($withEmpty) {
            array_unshift($options, array('value'=>'', 'label'=>Mage::helper('installation_gallery')->__('-- Please Select --')));
        }
		
        return $options;
    }
    
    public function getCategoryOptions()
    {
    	$options = array();
   		 
    	foreach ($this->getCollection() as $value => $label) {
    		$options[$value] = $label->getCategoryName();
        }
        
        return $options;
    }
     
}
