<?php

class Led_InstallationGallery_Model_Installation_Gallery_Item_Media 
    extends Mage_Core_Model_Abstract
{

    public function _construct()
    {
    	
        parent::_construct();
        $this->_init('installation_gallery/item_media');
    }

    /**
     * Remove images if an media object is deleted.
     *
     * @return $this
     */
    public function delete()
    {
        $filepath = Mage::getSingleton('installation_gallery/installation_gallery_item_media_config')
            ->getMediaPath($this->getPath());

        $result = parent::delete();

        unlink($filepath);
        
        return $result;
    }

    /**
     * Process media data before saving (ie adding creation en update time.
     *
     * @param Mage_Core_Model_Abstract $object
     * @return Mage_Cms_Model_Resource_Page
     */
    protected function _beforeSave()
    {
        parent::_beforeSave();

        // modify create / update dates
        if ($this->isObjectNew() && !$this->hasCreationTime()) {
            $this->setCreationTime(Mage::getSingleton('core/date')->gmtDate());
        }

        $this->setUpdateTime(Mage::getSingleton('core/date')->gmtDate());

        return $this;
    }


}
