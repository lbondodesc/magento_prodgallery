<?php

class Led_InstallationGallery_Model_Installation_Gallery_Item 
    extends Mage_Core_Model_Abstract
{

    /**
     * constants for the media gallery
     */
    const BASE_IMAGE = 'image';
    const SMALL_IMAGE = 'small_image';
    const THUMBNAIL = 'thumbnail';
    const NO_SELECTION = 'no_selection';
    /**
     * end constants for the media gallery
     */
    
    public function _construct()
    {
        parent::_construct();   
        // Set resource model
        $this->_init('installation_gallery/item');
        
    }
    

    public function save()
    {
        $res = parent::save();
        return $res;
    }

    public function getImageModel()
    {
        return Mage::getModel('installation_gallery/installation_gallery_item_image');
    }


   

    public function getImages()
    {
        if (!$this->hasData('images')) {
            $images = Mage::getModel('installation_gallery/installation_gallery_item_media')->getCollection()
                ->addFieldToFilter('installation_gallery_item_id', $this->getId())
                ->addFieldToFilter('disabled', 0)
                ->addOrder('position', Varien_Data_Collection_Db::SORT_ORDER_ASC)
                ;
            $this->setData('images', $images);
        }
        return $this->getData('images');
    }

    /**
     * Retrieve attributes for media gallery
     *
     * - Needed for media gallery
     * 
     * @return array
     */
    public function getMediaAttributes()
    {
        /**
         * In the product media gallery, the media attributes are fetched from
         * the eav attribute table. Attributes with type 'media_image'.
         *
        **/
        $media_attributes = array(
            self::BASE_IMAGE => 'Base Image',
            self::SMALL_IMAGE => 'Small Image',
            self::THUMBNAIL => 'Thumbnail',
        );

        return $media_attributes;
    }

    /**
     * Get's all the images linked to a reference and returns them, possible
     * merged with the given array.
     * 
     * @param array $images
     * @return json
     */
    public function getGallery($images = array())
    {
        if (!is_array($images))
        {
            $images = array();
        }
		  
        $mediaCollection = Mage::getModel('installation_gallery/installation_gallery_item_media')
            ->getCollection()
            ->addFieldToFilter('installation_gallery_item_id', $this->getId());

        foreach ($mediaCollection as $mediaItem)
        {
            $images[] = array(
                'value_id' => $mediaItem->getId(),
                'file' => $mediaItem->getPath(),
                'label' => $mediaItem->getLabel(),
                'position' => $mediaItem->getPosition(),
                'disabled' => $mediaItem->getDisabled(),
            );
        }

        return $images;
    }

}
