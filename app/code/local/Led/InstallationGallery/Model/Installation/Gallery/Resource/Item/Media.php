<?php

class Led_InstallationGallery_Model_Installation_Gallery_Resource_Item_Media 
    extends Mage_Core_Model_Resource_Db_Abstract
{

    public function _construct()
    {
        $this->_init('installation_gallery/installation_gallery_item_media', 'media_id');
    }

}