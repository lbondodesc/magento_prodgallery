<?php
class Led_InstallationGallery_Model_Installation_Gallery_Resource_Category_Collection 
    extends Mage_Core_Model_Resource_Db_Collection_Abstract
{

    public function _construct()
    {
        parent::_construct();
        $this->_init('installation_gallery/installation_gallery_category');
    }

}