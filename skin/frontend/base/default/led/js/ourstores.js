
jQuery(document).ready(function($){
    $('#location-list a').click(function(){
        var li = $(this).parent();
        var siblings = li.siblings();
        var sublist = li.children('ul');
        var active, pos, id;
        // li.children('ul').is(':visible')
        if (this.className.indexOf('active') != -1) {
            siblings.slideDown('fast');
            li.find('ul').not(sublist).hide();
            sublist.slideUp('fast');
            active = li.parent('ul').parent('li').children('a:first');
        } else {
            if (sublist.length > 0) {
                siblings.slideUp('fast');
                li.find('ul').hide()
                sublist.slideDown('fast');
            }
            active = $(this);
        }
        $('#location-list a').removeClass('active');
        if (active.length == 0) {
            active = $('#location-list>li>a:first');
        } else {
            active.addClass('active');
        }
        active = active.get(0);
        if (-1 != (pos = active.href.indexOf('#'))) {
            id = active.href.substr(pos);
            $('#location-descriptions').children().hide();
            $(id).fadeIn();
        }
        return false;
    });
    $('div.gallery').each(function(i){
        $(this).after('<div id="location_gallery_'+i+'" class="controls ui-helper-clearfix"></div>').cycle({
            fx:     'fade',
            speed:  500,
            timeout: 4000,
            pager:  '#location_gallery_'+i,
            before: function() { if (window.console) console.log(this.src); }
        })
    })
    .parent().hide().removeClass('hidden');
});

